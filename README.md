## Lab 7/8 - "Robot Sensing & Robot Maze" ##


### Objective
In this lab the concepts associated with the input capture features for the MSP430 were used together with IR sensing and motor controls. A single ultrasonic rangefinder is attached to a servo motor that can rotate. I will program my MSP430 to use the rangefinder to determine whether my mobile robot is approaching a wall in front or on one of its sides. Additionally, I will combine  results and program my robot to autonomously navigate through a maze. On the last day of the lab, each section will hold a competition to see who can solve the maze the fastest. The main goal of this lab is for you to have some fun with computer engineering! 

#### Required Functionality (complete)

Using the Timer_A subsystem to light LEDs based on the presence of a wall. The presence of a wall on the left side of the robot should light LED1 on your Launchpad board. The presence of a wall next to the right side should light LED2 on your Launchpad board. A wall in front should light both LEDs. Demonstrate that the LEDs do not light until the sensor comes into close proximity with a wall.

#### B Functionality (complete)
Full characterization of the sensor for the robot. Creating a table and graphical plot that shows the rangefinder pulse lengths for a variety of distances from a maze wall to the front/side of your robot. This table/graph must be generated for three different servo positions. Use these values to determine how your sensor/servo pair works so you can properly use them to solve the maze.

#### A Functionality (complete)
Controlling servo position with IR remote! Using remote buttons other than those I had used for the motors. Note: you still need to be able to get readings from the ultrasonic sensor after changing your servo position with the remote.

#### Bonus Functionality (complete)
Creating a standalone library for the ultrasonic code and releasing it on Bitbucket. This should be separate from the lab code. It should have a thoughtful interface and README, capable of being reused in the robot maze laboratory. This particular repository should be publicly accessible.

#### Maze Solver Functionality (complete)

You must write a program that autonomously navigates your robot through a maze (Figure 1) while meeting the following requirements:

- Your robot must always start at the home position.
- Your robot is considered successful only if it finds one of the three exits and moves partially out of the maze.
- A large portion of your grade depends on which door you exit.
	- Door 1 - Required Functionality
	- Door 2 - B Functionality
	- Door 3 - A Functionality (complete)
		- You cannot hit a wall!
- Bonus! Navigate from the A door back to the entrance using the same algorithm.
	- You cannot hit a wall!
- Your robot must solve the maze in less than three minutes.
- Your robot will be stopped if it touches the wall more than twice.
- Your robot must use the ultrasonic sensor to find its path through the maze.


### Prelab ###

For the input capture subsystem I will use TACCTL0 register with CM_3, TACCTL0 |= CCIE + CAP. Each TACCRx has two possible capture pins - CCIxA and CCIxB. The one being monitored is selectable by software.
If a capture occurs: - The TAR value is copied into the TACCRx register - The interrupt flag CCIFG is set

I will use TA0CCTL0 and pin 1.4 in order to generate PWM to control servo motor. I will send continuous pulse of ~5V to make it rotate from left to right using similar setup as for PWMs in Lab 6.

To capture sensor readings I will use pin 1.1 to generate required pulse of 10us. Pin 1.2 will be used to capture the echo back signal. For one detection two signal have to be captured, one on rising edge, one on falling edge. The timer value is copied into the TACCR0 register and the interrupt flag CCIFG is set.I will use synchronized mode for the capture. Overflow logic is provided in each capture register to indicate if a second capture was performed before the value from the first capture was read.

As for the interface to my sensor if will keep track of CCIFG flag and overflow bit to get the time of the captured signal. First flag would identify the start of the signal, then I will reset the flag, and wait for another occurrence of the flag to get end signal. The difference between two will be converted into the distance. Overflow bit will be safeguard to check if no readings are missed.

![circuit.png](https://bitbucket.org/repo/e4d6oG/images/698906073-circuit.png)

### Preliminary Design

The code from Lab 6- "Robot Motion" was used to control the motors in this lab as well. Additionally, previously written code for IR remote control was used in this lab.

### Lab Day 1

I have started this lab by accomplishing A functionality first. My remote was working and that was tested by reading the IR "packet" variable after a button click. I then used "bit-banging" method to control servo. Three different length signal were generated by simply pulling the output high, waiting for a certain time and then setting it to low. Picture below describes the movement of the servo in accordance to pulse lengths. Once subroutines for the signal generations were created, I used conditional statements from previous labs to call these servo movement functions based on the button presses.

### Lab Day 2 

To make the ultrasonic sensor work I ,first of all, needed to hard-wire this new attachment to the robot. Schematic for that can be seen in the figure under "Prelab" section. To make the sensor work, I needed to generate triggering signal of 10us. I have created subroutine for that by simply setting P1.1 high, delaying for 80 cycles, with is 10 us when using 8MHz clock, and then setting it to low. After triggering the sensor, it's output had to be captured. I used Timer_A subsystem's capture mode to do that. All the initialization and setup for that is being explained in the actual code files. The principle for that is simple: P1.2 is set as an input and will raises as a flag when signal to it is high. It will put TAR values to TA0CCR1 on the rising edge (signal start) and the falling edge (signal end). Te difference is then divided by 58 to get actual distance from the sensor. Once I was able to measure distance from the sensor to the wall, I could then use LED interface to complete the required functionality.  Additionally, B functionality was completed and the results can be seen below.

![Capture.PNG](https://bitbucket.org/repo/e4d6oG/images/2810034664-Capture.PNG)

### Lab Day 3 ( to include all extra working days )


After having sensor working correctly, I immediately moved to working on the "maze solver" functionality with the goal to make it through Door 3. The operation principle is simple: the sensor is moved to mid,left,right, mid positions (using servo) and measurements are taken at each position. The greatest distance to the wall is then the distance the robot needs to traverse. The robot turns to the side on which the wall is furthest and enables forward motor motion for certain amount of time (cycles) to close that distance. Once it stops, it will readjust its moving distance and position using exactly same principle. It will be done until robot goes through Door 3. The key for success in this methodology is perfectly straight( or close to it) forward movement and 90 degree turning angles.

### Lab Day 4 (competition)

The goal for this part of the lab was to go through the maze, exit doors 3, as fast as possible. Neither servo, nor the sensor were used in this part. It was heavily based on optimization and since my motors were very inconsistent it made this part very difficult. The robot did not finished the maze during official tries because it would not go straight or turn at right angles. It made through the maze couple times, but the overall result was not consistent enough. The program written for this part was just simply making the robot go forward for certain amount, the make appropriate turns and shoot through the doors #3.

![20151210_112949.jpg](https://bitbucket.org/repo/e4d6oG/images/968606201-20151210_112949.jpg)
### The Code

For A functionality the code including IR sensing was used, but after the functionality was completed, this portion of the code was removed to keep the main code easily readable and less complex. Additionally, LED interface was removed as well, after it was successfully tested. Remaining code from the Lab 7 was only servo and ultrasonic sensor subroutines, which then were used to solve the maze.

### Debugging

First issue that I encountered was that the capture mode was not capturing anything at all. The trigger output and echo signal were tested on the oscilloscope to conclude that the sensor was working properly, but the software wasn't. It was then discovered that P1.2 connected to echo pin on the servo was set to the output instead of the input mode. Easy fix, though very time consuming to locate the issue.

Another issue occurred with controlling the servo. After sending correct pulse, which was tested with oscilloscope, the servo was not moving. It was then realized that the same signal has to be sent multiple times, go give servo enough time to reach the desired position. It created 'for' loop which sent the signal 15-20 times.

The biggest issue in this lab was to make robot's forward motion straight and turns angled appropriately. The TA1CCR1 and TA1CCR2 values were changed many times to get to the acceptable movement functionality. Additionally, changing batteries would lead to the change in the movement. To make straight movement smoother, functionality that make robot slowly stop was implemented, instead of letting it slam on breaks immediately, which leads to some movement discrepancies.

### Testing methodology 

First of all, it had to be tested that the servo is turning at the right angles and right order. Specific length signal for one side (left) was sent to see where the servo would position itself. Initial it would not make all the way to the left (90 degrees), thus signal length was shortened to get the right angle. For the middle position default signal, from the servo image, was correct. Additionally, right servo position needed increased length signal, compared to the default signal.

Secondly, the ultrasonic sensor had to be tested multiple times to see if it works properly and to turn MSP430 clock cycles into actual distance in centimeters. To test the sensor I mainly used breaking point just after the full signal from the sensor was received. When it was converted to centimeters, I have measured actual distance to the wall with a ruler. The measurement errors from multiple tests were insignificant, meaning that the sensor was very accurate.

Finally, to make the robot solve the maze and reach doors #3, multiple tests along the maze were done. Firstly, optimal speed was chosen to traverse straight path in the maze. Secondly, multiple tests were done to choose and calculate appropriate turning angles for both right and left turns. Finally, robot ran the maze multiple times to make sure the code works appropriately and as consistently as possible.

### Conclusion

To sum up, all the objectives were completed successful, except one: The main goal of this lab is for you to have some fun with computer engineering!" The main reason behind that is bad motors used. To improve this lab experience more consistent motors should be used. Motors that do not change their revolutions per minutes based on slight changes in battery capacity or changing friction for every run. Additionally, instead of "bubbles" used in the front or back could be replaced with some time of wheels as well. On the other hand, it can be concluded that this lab helped to understand interrupts and timing subsystems in much greater depth. Subsequently, dealing with the servo controls was good and useful experience as well.