//-----------------------------------------------------------------
// Name:	C2C Laurynas Galdikas
// File:	main.c
// Date:	10 December 2015
// Purp:	This file contains code for IR remote control package
//			reading and iterpreting it to contol a robot using MSP430 GPIOs.
// Doc:		None
//-----------------------------------------------------------------
//-----------------------------------------------------------------
#include <msp430.h>
#include "start5.h"

int16	packetData[48];
int8	packetIndex = 0;
int32	packet;
char 	loop = 15;
char	loop_start;
int		signal_start, signal_end,full_distance,difference;
int 	facing_side, change_side, distance, current_dist, next_dist;


char turn;
char smallTurn = 2; // Value corresponding to a small radius for turning
char bigTurn = 5;   // 90 degree turn

//-----------------------------------
// This function sends 10us (80cycles/8MHz) signal to the sensor
// connected to pin 1.1
void trigger(){

	P1OUT |= BIT1;
	__delay_cycles(80);
	P1OUT &=~ BIT1;
}


//-----------------------------------
// Following servoX() functions sends appropriate length signal to move servo
// to a certain location

void servoLeft (void){

	// Loop to send specific signal couple times to physically allow servo to move.
	for(loop_start = 0; loop_start < loop ;loop_start++){
		P1OUT |= BIT4;
		__delay_cycles(20000);
		P1OUT &=~BIT4;
	}
	facing_side = FACE_LEFT; // Indicator to keep track where the sensor is facing
}

void servoMid (void){

	for(loop_start = 0; loop_start < loop ;loop_start++){
		P1OUT |= BIT4;
		__delay_cycles(12000);
		P1OUT &=~BIT4;
	}
	facing_side = FACE_MID;
}

void servoRight (void){

	for(loop_start = 0; loop_start < loop ;loop_start++){
		P1OUT |= BIT4;
		__delay_cycles(4000);
		P1OUT &=~BIT4;
	}
	facing_side = FACE_RIGHT;
}


void stop(void){

    __delay_cycles(1000000);
    P2OUT = 0;
    TA1CTL &= ~MC0;
    TA1CCR2 = 1000;
    TA1CCR1 = 1000;
    TA1CTL |= MC_1;
}

void moveForward(void){

	for ( turn = 0; turn < distance ; turn++){


		__delay_cycles(1000000); // 1/8 ms delay.
		P2OUT &= ~BIT0;		// Send 0V to Pin 2.0
		P2OUT &= ~BIT5;
		TA1CTL &= ~MC0;		// Stop the clock
		TA1CCR2 = 295;		// Duty Cycle of ~7.9 V  for the left motor
		TA1CCR1 = 283;		// Right motor
		TA1CTL |= MC_1;		// Turn on the clock}
	}
	turn = 0 ;
	
	// Logic used to gently stop the robot.
	for ( turn = 0; turn < 4 ; turn++){

		__delay_cycles(1000000);
	    P2OUT &= ~BIT0;		// Send 0V to Pin 2.0
	    P2OUT &= ~BIT5;
	    TA1CTL &= ~MC0;		// Stop the clock
	    TA1CCR2 = 625;		// Duty Cycle of 63-68% for the left motor
	    TA1CCR1 = 683;		// and for the  Right motor
	    TA1CTL |= MC_1;		// Turn on the clock}

	}
	stop();
}
void moveBack(void){
    __delay_cycles(1000000);
    P2OUT |= BIT0;
    P2OUT |= BIT5;		// Send 12V to Pin 2.5
    TA1CTL &= ~MC0;
    TA1CCR2 = 500;		// Send 6V to Pin 2.4 to create Potential Difference of 6V
    TA1CCR1 = 500;
    TA1CTL |= MC_1;

}

void turnRight(void){


	for ( turn = 0; turn < bigTurn; turn++){

    __delay_cycles(1000000);
    P2OUT |= BIT0;	
    P2OUT &= ~BIT5;
    TA1CTL &= ~MC0;
    TA1CCR1 = 460;
    TA1CCR2 = 675;
    TA1CTL |= MC_1;
	}
	stop();

}

void smallRight(void){


	for ( turn = 0; turn < smallTurn ; turn++){

    __delay_cycles(1000000);
    P2OUT |= BIT0;	
    P2OUT &= ~BIT5;
    TA1CTL &= ~MC0;
    TA1CCR1 = 500;
    TA1CCR2 = 635;
    TA1CTL |= MC_1;
	}
	stop();

}
void turnLeft(void){

	for ( turn = 0; turn < bigTurn ; turn++){
    __delay_cycles(1000000);
    P2OUT &= ~BIT0;
    P2OUT |= BIT5;
    TA1CTL &= ~MC0;
    TA1CCR2 = 480;
    TA1CCR1 = 650;
    TA1CTL |= MC_1;
	}
    stop();
}

void smallLeft(void){

	for ( turn = 0; turn < smallTurn ; turn++){
    __delay_cycles(1000000);
    P2OUT &= ~BIT0;
    P2OUT |= BIT5;
    TA1CTL &= ~MC0;
    TA1CCR2 = 480;
    TA1CCR1 = 625;
    TA1CTL |= MC_1;
	}
    stop();
}

// ----------------------------------------------------------------
// Turns the robot to face the wall which is the furthest.

void adjustPosition (void){

	if (change_side == FACE_LEFT){

		turnLeft();
	}

	if (change_side == FACE_RIGHT){

		turnRight();
	}
}

// --------------------------------------------------------------
// Determines which side has the longest distance to the wall.

void adjustFace(void){

	// Starts with middle distance being the longest at first
	if (facing_side == FACE_MID){

		current_dist = full_distance;
		change_side = facing_side;	 // Variable to used to adjust robots position.
	}

	else if (facing_side == FACE_LEFT && full_distance > current_dist){

		current_dist = full_distance;
		change_side = facing_side;
	}

	else if (facing_side == FACE_RIGHT && full_distance > current_dist){

		current_dist = full_distance;
		change_side = facing_side;
	}
}
// --------------------------------------------------------------------------
// This function control the servo and take three distance measurement at each
// servo position mid, left, right. Timing was very important factor to get
// accurate measurement, thus it takes long time to through this subroutine.
void servoRoutine(void){

	servoMid();					// Turns the servo/positions the sensor
	__delay_cycles(8000000);
	trigger();					// Sends 10us signal to the sensor
	__delay_cycles(500000);
	trigger();
	__delay_cycles(500000);
	trigger();
	__delay_cycles(500000);
	adjustFace();				// Determines if this side has longer distance than previous position
	__delay_cycles(8000000);

	// Same logic for left position 
	servoLeft();
	__delay_cycles(4000000);
	trigger();
	__delay_cycles(500000);
	trigger();
	__delay_cycles(500000);
	trigger();
	__delay_cycles(500000);
	adjustFace();
	__delay_cycles(8000000);

	// Same logic for right position 
	servoRight();
	__delay_cycles(8000000);
	trigger();
	__delay_cycles(500000);
	trigger();
	__delay_cycles(500000);
	trigger();
	__delay_cycles(500000);
	adjustFace();
	__delay_cycles(8000000);


	adjustPosition();			// Turns the robot to the side where the greatest distance was found.
	servoMid();					// Positions the servo back to the middle.
	__delay_cycles(8000000);


}

void main(void)
{
	initSensor();
	initRobot();
	initMSP430();

	__enable_interrupt();
	
	while(1){

		servoRoutine();
		distance = current_dist/13; // Variable used to determine for how long should motors be turned on.
		moveForward();
		__delay_cycles(8000000);

		} // end infinite loop
} // end main
// -----------------------------------------------------------------------
// In order to decode IR packets, the MSP430 needs to be configured to
// tell time and generate interrupts on positive going edges.  The
// edge sensitivity is used to detect the first incoming IR packet.
// The P2.6 pin change ISR will then toggle the edge sensitivity of
// the interrupt in order to measure the times of the high and low
// pulses arriving from the IR decoder.
//
// The timer must be enabled so that we can tell how long the pulses
// last.  In some degenerate cases, we will need to generate a interrupt
// when the timer rolls over.  This will indicate the end of a packet
// and will be used to alert main that we have a new packet.
// -----------------------------------------------------------------------
void initSensor ( void ) {

 	WDTCTL = WDTPW|WDTHOLD;           // stop the watchdog timer

    P1DIR |= BIT1;    // TA0CCR1 on P2.1 and TA1CCR2 on P2.4
    P1DIR &=~ BIT2;
    P1SEL |= BIT2;
    P1SEL2 &=~ BIT2;

    TACTL &= ~TAIFG;

    TA0CCTL1 |= CAP|CM_3|SCS|CCIE|CCIS_0;

}

void initRobot ( void ) {

 	WDTCTL = WDTPW|WDTHOLD;           // stop the watchdog timer

    P2DIR |= BIT1|BIT0|BIT4|BIT5;    // TA1CCR1 on P2.1 and TA1CCR2 on P2.4
    P2SEL |= BIT1|BIT4;
    P2OUT = 0;

    TA1CTL |= TASSEL_2|MC_1|ID_0;           // configure for SMCLK

    P1DIR |= BIT5|BIT4;				// Pin 1.0 used for EN functionality for the Driver chip.
           //use LED to indicate duty cycle has toggled

    P1OUT |= BIT5;
    P1REN |= BIT3;
    P1OUT |= BIT3;


    P1OUT &= ~BIT4;

    TA1CCR0 = 1000;              // Set Duty Cycle to 0%
    TA1CCR1 = 1000;
    TA1CCR2 = 1000;
    TA1CCTL1 |= OUTMOD_3;        // set TACCTL1 to Set / Reset mode
    TA1CCTL2 |= OUTMOD_3;		 // set TACCTL2 to Set / Reset mode

}
void initMSP430() {

	WDTCTL=WDTPW+WDTHOLD; 		// stop WD

	BCSCTL1 = CALBC1_8MHZ;
	DCOCTL = CALDCO_8MHZ;

	P2DIR &= ~BIT6;					// Set up P2.6 as GPIO not XIN	chen 					// This action takes
	P2SEL &= ~BIT6;
	P2SEL2 &= ~BIT6;

	P2IFG &= ~BIT6;						// Clear any interrupt flag on P2.3
	P2IE  |= BIT6;						// Enable P2.3 interrupt

	HIGH_2_LOW;

    P1DIR |= BIT0|BIT6;                // check the header out.  P2IES changed.
    P1OUT &=~ BIT0;
    P1OUT &=~ BIT6;

	TA0R = 0;							// ensure TAR is clear
	TA0CCR0 = 0xFDE8;					// create a ~65 ms roll-over period with TA0CCR0
	TACTL &= ~TAIFG;					// clear flag before enabling interrupts = good practice
	TACTL |= ID_3 | TASSEL_2 | MC_1 | TAIE;	 // Use 1:8 prescalar off SMCLK and enable interrupts

	_enable_interrupt();
}

// -----------------------------------------------------------------------
// Since the IR decoder is connected to P2.6, we want an interrupt
// to occur every time that the pin changes - this will occur on
// a positive edge and a negative edge.
//
// Negative Edge:
// The negative edge is associated with end of the logic 1 half-bit and
// the start of the logic 0 half of the bit.  The timer contains the
// duration of the logic 1 pulse, so we'll pull that out, process it
// and store the bit in the global irPacket variable. Going forward there
// is really nothing interesting that happens in this period, because all
// the logic 0 half-bits have the same period.  So we will turn off
// the timer interrupts and wait for the next (positive) edge on P2.6
//
// Positive Edge:
// The positive edge is associated with the end of the logic 0 half-bit
// and the start of the logic 1 half-bit.  There is nothing to do in
// terms of the logic 0 half bit because it does not encode any useful
// information.  On the other hand, we going into the logic 1 half of the bit
// and the portion which determines the bit value, the start of the
// packet, or if the timer rolls over, the end of the ir packet.
// Since the duration of this half-bit determines the outcome
// we will turn on the timer and its associated interrupt.
// -----------------------------------------------------------------------

#pragma vector = TIMER0_A1_VECTOR			// This is from the MSP430G2553.h file
__interrupt void pulseMeasurement (void) {

	if(TA0CCTL1 & CCI){

		signal_start = TA0CCR1;
	}

	if (~(TA0CCTL1 & CCI)){

		signal_end = TA0CCR1;
	}

	difference = signal_end-signal_start;
	full_distance = (signal_end-signal_start)/58 ;


	TA0CCTL1 &=~ CCIFG;
	TACTL &= ~TAIFG;
}
