## Lab 7 - "Robot Sensing" 2 ##


### Prelab ###

For the input capture subsystem I will use TACCTL0 register with CM_3, TACCTL0 |= CCIE + CAP. Each TACCRx has two possible capture pins - CCIxA and CCIxB. The one being monitored is selectable by software.
If a capture occurs: - The TAR value is copied into the TACCRx register - The interrupt flag CCIFG is set

I will use TA0CCTL0 and pin 1.1 in order to generate PWM to control servo motor. I will send continuous pulse of ~5V to make it rotate from left to right using similar setup as for PWMs in Lab 6.

To capture sensor readings I will use pin 1.2 to generate required pulse of 10us. Pin 1.4 will be used to capture the echo back signal. For one detection two signal have to be captured, one on rising edge, one on falling edge. The timer value is copied into the TACCR0 register and the interrupt flag CCIFG is set.I will use synchronized mode for the capture. Overflow logic is provided in each capture register to indicate if a second capture was performed before the value from the first capture was read.

As for the interface to my sensor if will keep track of CCIFG flag and overflow bit to get the time of the captured signal. First flag would identify the start of the signal, then I will reset the flag, and wait for another occurrence of the flag to get end signal. The difference between two will be converted into the distance. Overflow bit will be safeguard to check if no readings are missed.