//-----------------------------------------------------------------
// Name:	C2C Laurynas Galdikas
// File:	start5.h
// Date:	29 November 2015
// Purp:	The header file for decoding of an IR packet and using the IR signal to control ech-sketch game.
// Doc:		None
//-----------------------------------------------------------------
//-----------------------------------------------------------------
// Page 76 : MSP430 Optimizing C/C++ Compiler v 4.3 User's Guide
//-----------------------------------------------------------------
typedef		unsigned char		int8;
typedef		unsigned short		int16;
typedef		unsigned long		int32;
typedef		unsigned long long	int64;

#define		TRUE				1
#define		FALSE				0
#define		IR_LENGTH			33
#define		MAX_Y				320
#define		MAX_X				240
#define		BLOCK_STEP			10
#define		NEGATIVE			65516
#define		FACE_LEFT			13
#define		FACE_MID			14
#define		FACE_RIGHT			15
//-----------------------------------------------------------------
// Function prototypes found in lab5.c
//-----------------------------------------------------------------
void initMSP430();
__interrupt void pinChange (void);
__interrupt void timerOverflow (void);


//-----------------------------------------------------------------
// Each PxIES bit selects the interrupt edge for the corresponding I/O pin.
//	Bit = 0: The PxIFGx flag is set with a low-to-high transition
//	Bit = 1: The PxIFGx flag is set with a high-to-low transition
//-----------------------------------------------------------------

#define		IR_PIN			(P2IN & BIT6)
#define		HIGH_2_LOW		P2IES |= BIT6
#define		LOW_2_HIGH		P2IES &= ~BIT6


#define		averageLogic0Pulse	0x01EC
#define		averageLogic1Pulse	0x064A
#define		averageStartPulse	0x110F
#define		minLogic0Pulse		averageLogic0Pulse - 100
#define		maxLogic0Pulse		averageLogic0Pulse + 100
#define		minLogic1Pulse		averageLogic1Pulse - 100
#define		maxLogic1Pulse		averageLogic1Pulse + 100
#define		minStartPulse		averageStartPulse - 100
#define		maxStartPulse		averageStartPulse + 100

#define		UP		0x08F7847B
#define		DOWN	0x08F744BB
#define		LEFT	0x08F7F807
#define		RIGHT	0x08F704FB
#define		OK		0x08F7D42B
#define		i_echo	0x08F7C23D
#define		CH_UP	0x08F7946B
#define		CH_DW	0x08F754AB
#define		OPT_1	0x08F7748B
#define		OPT_2	0x08F7F40B
#define		OPT_3	0x08F70CF3
