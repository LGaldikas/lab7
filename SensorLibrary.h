/*
 * SensorLibrary.h
 *
 *  Created on: Dec 4, 2015
 *      Author: C17L.Galdikas
 */

#ifndef SENSORLIBRARY_H_
#define SENSORLIBRARY_H_

#include <msp430.h>
#include "start5.h"

char 	loop = 5;
char	loop_start,facing_side;
int		signal_start, signal_end,full_signal,difference;

//-----------------------------------
// This function sends 10us (80cycles/8MHz) signal to the sensor
// connected to pin 1.1
void trigger(){

	P1OUT |= BIT1;
	__delay_cycles(80);
	P1OUT &=~ BIT1;
}


//-----------------------------------
// Following servoX() functions sends apropriate length signal to move servo
// to a certain location

void servoLeft (void){

	// Loop to send specific signal couple times to physically allow servo to move.
	for(loop_start = 0; loop_start < loop ;loop_start++){
		P1OUT |= BIT4;
		__delay_cycles(20000);
		P1OUT &=~BIT4;
	}
	facing_side = LEFT; // Indicator to keep track where the sensor is facing
}

void servoMid (void){

	for(loop_start = 0; loop_start < loop ;loop_start++){
		P1OUT |= BIT4;
		__delay_cycles(12000);
		P1OUT &=~BIT4;
	}
	facing_side = MID;
}

void servoRight (void){

	for(loop_start = 0; loop_start < loop ;loop_start++){
		P1OUT |= BIT4;
		__delay_cycles(4000);
		P1OUT &=~BIT4;
	}
	facing_side = RIGHT;
}

// --------------------------------------------------
// Function to initialize the sensor.
void initSensor ( void ) {

 	WDTCTL = WDTPW|WDTHOLD;           // stop the watchdog timer

    P1DIR |= BIT1;    	// Output mode for pin1.1 (trigger)
    P1DIR &=~ BIT2;		// Pin 1.2 configure as input able to capture CCIA
    P1SEL |= BIT2;
    P1SEL2 &=~ BIT2;

    TACTL &= ~TAIFG;

    TA0CCTL1 |= CAP|CM_3|SCS|CCIE|CCIS_0;	// Clock setup

}

// ----------------------------------------------------------------------------
// ISR function to capture the sensor echo signal

#pragma vector = TIMER0_A1_VECTOR			// This is from the MSP430G2553.h file
__interrupt void pulseMeasurement (void) {

	if(TA0CCTL1 & CCI){ // Reads the start of the signal when CCI is high

		signal_start = TA0CCR1;	// Put cycles' value to a variable
	}

	if (~(TA0CCTL1 & CCI)){ // Reads the start of the signal when CCI is low

		signal_end = TA0CCR1;
	}

	// Logic to convert clock cycle to time for the echoed signal
	difference = signal_end-signal_start;
	full_signal = (signal_end-signal_start)/58 ;

	TA0CCTL1 &=~ CCIFG;
	TACTL &= ~TAIFG;
}

#endif /* SENSORLIBRARY_H_ */
